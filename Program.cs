﻿// See https://aka.ms/new-console-template for more information
using RegexValidation;

Console.WriteLine("Hello, World!");

Console.WriteLine("Enter Email");
string email = Console.ReadLine();
if (RegVal.valEmail(email))
{
    Console.WriteLine("Good Email");
}
else
{
    Console.WriteLine("Bad Email");
}

Console.WriteLine("Enter Password");
string pass = Console.ReadLine();
if (RegVal.valPass(pass))
{
    Console.WriteLine("Good Password");
}
else
{
    Console.WriteLine("Bad Password");
}