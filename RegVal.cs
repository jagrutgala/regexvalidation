﻿using System;
using System.Text.RegularExpressions;
namespace RegexValidation
{
    internal class RegVal
    {
        public static bool valPass(string pass)
        {
            string regexStr = @"^[a-zA-Z0-9!@#$%^&*]{8,}$"; // 8 character password with no whitespace
            return Regex.IsMatch(pass, regexStr);
        }

        public static bool valEmail(string email)
        {
            string regexStr = @"^([a-zA-Z]+[a-zA-Z0-9]*){5,}@([a-zA-z]){3,}.com$";
            // must start with letter numbers optional
            // minimum 5 chars before @
            // must include @
            // only letters allowed in domain (i.e. after @)
            // domain must end with .com
            return Regex.IsMatch(email, regexStr);
        }

    }
}
